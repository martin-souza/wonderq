exports.config = {
    "listenPort": 2222,
    "saveLog": true,
    "queue": {
      "maxSize": 1000,
      "messages": {
        "expirationTime": 5000,
        "idSize": 20,
      }
    }
  }  


