import { config } from "./../config.js";
import actions from "./actions.js";

export default {
  test0: async () => {
    console.log("Cleaning queue for test...");
    const response = await actions.emptyQueue();
    if (response && response.data.message === "Queue is empty") {
      console.log("Test 0 passed");
    } else {
      console.log("Test 0 fail");
    }
  },

  test1: async () => {
    //save the returned id
    const idMessage1 = await actions.pushMessage();
    const idMessage2 = await actions.pushMessage();

    //test fifo
    const message1 = await actions.getMessage();
    if (message1 && idMessage1 === message1["_id"]) {
      console.log("Test 1.1 passed");
    } else {
      console.log("Test 1.1 fail");
    }
    //mesage 2
    const message2 = await actions.getMessage();
    if (message2 && idMessage2 === message2["_id"]) {
      console.log("Test 1.2 passed");
    } else {
      console.log("Test 1.2 fail");
    }

    //mesage 3
    const response = await actions.getMessage();
    if (response && response.error && response.code === "324") {
      console.log("Test 1.3 passed");
    } else {
      console.log("Test 1.3 fail");
    }
  },

  test2: async () => {
    const messagesToAdd = 1000;
    let i;

    actions.emptyQueue();

    for (i = 0; i < messagesToAdd; i++) {
      await actions.pushMessage();
    }

    let queueState = await actions.getQueueState();
    if (queueState && queueState.size === messagesToAdd) {
      console.log("Test 2.1 passed");
    } else {
      console.log("Test 2.1 fail");
    }

    for (i = 0; i < messagesToAdd / 2; i++) {
      await actions.getMessage();
    }

    queueState = await actions.getQueueState();
    if (queueState && queueState.size === messagesToAdd / 2) {
      console.log("Test 2.2 passed");
    } else {
      console.log("Test 2.2 fail");
    }

    for (i = 0; i < messagesToAdd / 2; i++) {
      await actions.getMessage();
    }

    queueState = await actions.getQueueState();
    //console.log(queueState);
    if (queueState && queueState.size === 0) {
      console.log("Test 2.3 passed");
    } else {
      console.log("Test 2.3 fail");
    }
  },

  //Check if size queue works
  test3: async () => {
    const messagesToAdd = config.queue.maxSize;
    let i;

    for (i = 0; i < messagesToAdd; i++) {
      await actions.pushMessage();
    }

    const response = await actions.pushMessage();
    if (response && response.error && response.code === "987") {
      console.log("Test 3.1 passed");
    } else {
      console.log("Test 3.1 fail");
    }
  },

  //Check if clean the queue works
  test4: async () => {
    let response = await actions.emptyQueue();
    if (response && response.data.message === "Queue is empty") {
      console.log("Test 4.1 passed");
    } else {
      console.log("Test 4.1 fail");
    }

    const messagesToAdd = 5;
    for (let i = 0; i < messagesToAdd; i++) {
      await actions.pushMessage();
    }

    let queueState = await actions.getQueueState();
    if (queueState && queueState.size === messagesToAdd) {
      console.log("Test 4.2 passed");
    } else {
      console.log("Test 4.2 fail");
    }

    response = await actions.emptyQueue();
    if (response && response.data.message === "Queue is empty") {
      console.log("Test 4.3 passed");
    } else {
      console.log("Test 4.3 fail");
    }

    queueState = null;
    queueState = await actions.getQueueState();
    if (queueState && queueState.size === 0) {
      console.log("Test 4.4 passed");
    } else {
      console.log("Test 4.4 fail");
    }
  },

  //Check if checkout time works, and  auto re enqueue works
  test5: async () => {
    
    // clean queue
    let response = await actions.emptyQueue();
    if (response && response.data.message === "Queue is empty") {
      console.log("Test 5.1 passed");
    } else {
      console.log("Test 5.1 fail");
    }

    
    await actions.pushMessage();

    let queueState = await actions.getQueueState();
    if (queueState && queueState.size === 1) {
      console.log("Test 5.2 passed");
    } else {
      console.log("Test 5.2 fail");
    }

    const message1 = await actions.getMessage();
    if (message1) {
      console.log("Test 5.3 passed");
    } else {
      console.log("Test 5.3 fail");
    }

    response = await actions.getMessage();

    if (response.error) {
      console.log("Test 5.4 passed");
    } else {
      console.log("Test 5.4 fail");
    }

    setTimeout(async () => {
      //ths will be executed one second after the expiration time
      queueState = await actions.getQueueState();
      if (queueState && queueState.size === 1) {
        console.log("Test 5.5 passed");
      } else {
        console.log("Test 5.5 fail");
      }
    }, config.queue.messages.expirationTime + 1000);
  },
};
