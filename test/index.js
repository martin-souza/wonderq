import test from "./test"

const main = async () => {
    await test.test0();
    await test.test1();
    await test.test2();
    await test.test3();
    await test.test4();
    await test.test5();
  };
  
  main();
  