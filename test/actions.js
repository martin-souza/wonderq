import axios from "axios";

let db ={}

export default {
  //sends a recuest to push a message with a random hash as data, when WonderQ does it,
  // it responses with the id of the new message, the dupla id-hash is saved in the object db
  
  ////// the idea of this was to check if the data of the message is the same that was sent when it was created
  
  pushMessage: async () => {
    try {
      const hash = Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 5);
      const resp = await axios.post("http://localhost:2222/messages/", {
        data: { hash: hash },
      });

      db[resp.data] = hash;

      return resp.data;
    } catch (err) {
      // Handle Error Here
      console.log(err)
      console.error(
        "Start WonderQ service in other terminal with: npm run start"
      );
    }
  },

  getQueueState: async () => {
    try {
      const resp = await axios.get("http://localhost:2222/queue/");
      //console.log(resp.data);
      return resp.data;
    } catch (err) {
      // Handle Error Here
      console.error(
        "Start WonderQ service in other terminal with: npm run start"
      );
    }
  },

  emptyQueue: async () => {
    try {
      const resp = await axios.delete("http://localhost:2222/queue/");
      return resp;
    } catch (err) {
      // Handle Error Here
      console.error(
        "Start WonderQ service in other terminal with: npm run start"
      );
    }
  },

  getMessage: async () => {
    try {
      const resp = await axios.get("http://localhost:2222/messages/");
      return resp.data;
    } catch (err) {
      // Handle Error Here
      console.error(
        "Start WonderQ service in other terminal with: npm run start"
      );
    }
  },
};
