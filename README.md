# README #

WonderQ - Design a simple queuing system

Look at Amazon's Simple Queuing System for some guidance (which is what we actually use): http://goo.gl/Bn8qaD

We want you to design something similar, but simpler:

WonderQ is a broker that allows producers to write to it, and consumers to read from it. It runs on a single server. Whenever a producer writes to WonderQ, a message ID is generated and returned as confirmation. Whenever a consumer polls WonderQ for new messages, it can get those messages. These messages should NOT be available for pending by any other consumer that may be concurrently accessing WonderQ.

NOTE that, when a consumer gets a set of messages, it must notify WonderQ that it has processed each message (individually). This deletes that message from the WonderQ database. If a message is received by a consumer but NOT marked as processed within a configurable amount of time, the message then becomes available to any consumer requesting again.

Tasks:

• Build a module that represents WonderQ. Please store data in-memory using Javascript data structures rather than using a database or Redis.
• Create the REST API endpoints that producers/consumers could use to generate and consume messages

Use the most efficient data structures and API endpoints possible to support a high volume of messages.

• Write documentation for your API endpoints. Talk about their inputs/outputs, formats, methods, responses, etc

• Discuss in writing (in your Github README): what steps would you need to take in order to scale this system to make it production ready for very high volume? What are potential issues in production that might arise and how would you go about implementing solutions? For each of the previous questions, which tools/technologies would you use and why? Please be detailed.

• Use Node.js and ES6/ES7 as that is what we use (you may also use Typescript, but it is optional)

• Add tests for the critical functionality of WonderQ

### Prerequisits ###

Install 

* Node
* NPM

### Start service ###

Some variables can be change on the json saved in config.js
* change expirationTime to set the time in milliseconds in wich a message becomes available to any consumer requesting after be received by a consumer but NOT marked as processed

For start QWonder service run in terminal:

npm install
npm run start

### API endpoints ###

###### Consumers ######

Send a get request to path /messages/ to get the next available message on Queue,
 this message won't be able to be read from other consumers for 5000 sec. (you can change this time in config.js,  queue.message.expirationTime)
 If this message isn't deleted from the consumer in 5000 seconds the message will be re-enqueued to be processed by a consumer-available.
e.g.
>requests.get('https://localhost:2222/messages/')
>{
    "_data": {
        "key": "value",
    },
    "_id": "0.y6t6upigd0k",
    "_creationDate": 1601629087999
}


Send a :delete request to path /messages/:id to delete the message after being processed. 
Is useful to set a message as processed.
e.g.
>requests.delete('https://localhost:2222/messages/0.y6t6upigd0k/') to delete message with id "0.y6t6upigd0k" previously created in the las description. This message only can be deleted if was previously dequeued by the last method described.

###### Producers ######

Send a post request with an object as payload in body to path /messages/ to create and enqueue a new message.
As response you will receive a string that is the id of the new message 
e.g.
>requests.post('https://localhost:2222/messages/', data = {'key':'value'})   
"0.y6t6upigd0k"

Send a get request to path /queue/ to get an object with information about the queue
e.g.
>requests.get('https://localhost:2222/queue/')
>
{
    size: 1,
    nextMessaje: {
        "_data": {
            "key": "value"
        },
        "_id": "0.y6t6upigd0k",
        "_creationDate": 1601629087999
    }
}

Send a delete request to path /queue/ to empty the queue
e.g.
>requests.delete('https://localhost:2222/queue/')
>
{
    "message": "Queue is empty"
}

### LOG ###

Log is save in debug.log

### Test running ###

For start tests run in terminal:

npm run test

Test is script that will be in charge to measure the behavior of qWonder sending requests to the service, simulating producers and consumers.

### Questions ###

A: What steps would you need to take in order to scale this system to make it production ready for very high volume?
R: I would run the service with some advanced process manager for production Node.js applications with load balancer, logs facility, startup script, microservice management, something like pm2. It would take the heartbeat of the process, if it falls down for some unexpected reason, p2m will start the service again. I also would implement record in a database, if the principal process crashes, is good idea to rehydrate the queue with the messages lost. 
Optimice the search of messages in lists. A lineal search of order n could be reduced using arrays to O(1).
Delete all unused dependencies and modules

A: What are potential issues in production that might arise and how would you go about implementing solutions?
R: Queue is allocated on RAM, so if the queue grows too much the RAM will be a bottleneck, so I would add mode RAM the server. A great tool for that is virtualization technology, cloud services as Azure and AWS offer cheap and easy solutions for that. 
Is important to have good infrastructure to handle all requests and increasing data sizes and creating a CDN for faster delivery times.
Efficient indexing and multithreading would allow the application to handle many requests.