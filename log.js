import fs from "fs";
import util from "util";

export default () => {
    var log_file = fs.createWriteStream(__dirname + "/debug.log", {
      flags: "w",
    });
    var log_stdout = process.stdout;

    console.log = function (d) {
      log_file.write(util.format(d) + "\n");
      log_stdout.write(util.format(d) + "\n");
    };
};
