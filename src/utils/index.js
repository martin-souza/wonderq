export default  {
  //Find message by id
  findMessage: (id, arr) => {
    let message = arr.find((message) => {
      return message["_id"] === id;
    });

    if (message) {
      return message;
    } else {
      return null;
    }
  },

  //Deletes the messages
  deleteMessage: (msg, arr) => {
    console.log(arr);

    //this deletes the element in the array
    const index = arr.indexOf(msg);
    if (index > -1) {
      arr.splice(index, 1);
    }

    return msg._id + " has been processed";
  },
};
