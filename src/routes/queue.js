import { Router } from "express";
import { queueController } from "../controllers";

const router = Router();

router.get("/", (req, res) => {
  const actionResponse = queueController.getState();

  if (actionResponse){
    return res.send(actionResponse);
  }else{
    res.status(500).send({ error: 'Something failed!' });
  }
});

router.delete("/", (req, res) => {
  const actionResponse = queueController.clear();

  if (actionResponse){
    return res.send(actionResponse);
  }else{
    res.status(500).send({ error: 'Something failed!' });
  }
});

export default router;
