import { Router } from "express";
import { queueController } from "../controllers";

const router = Router();

//for add a new messaje
router.post("/", (req, res) => {
  const { data } = req.body;
  const actionResponse = queueController.enqueue(data);

  if (actionResponse) {
    return res.send(actionResponse);
  } else {
    return res.status(500).send({ error: "Something failed!" });
  }
});

//for get the next message in fifo
router.get("/", (req, res) => {
  const actionResponse = queueController.dequeue();

  if (actionResponse) {
    return res.status(200).send(actionResponse);
  } else {
    return res.status(500).send({ error: actionResponse });
  }
});

//deletes the message in the queue only when it is procesing
router.delete("/:id", (req, res) => {
  const { id } = req.params;

  const actionResponse = queueController.delete(id);

  if (actionResponse) {
    return res.send(actionResponse);
  } else {
    return res.status(500).send({ error: actionResponse });
  }
});

export default router;
