import messages from "./messages";
import queue from "./queue";

export default {
    messages,
    queue
};
