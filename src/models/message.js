import { config } from "./../../config";
import { queueController } from "../controllers";
//import {findMessage, } from "./../utils";
import utils from "./../utils";

export default class Message {
  //Creates a new queue instance and initializes the underlying data structure
  constructor(data, id) {
    this._data = data;
    this._id = id ? id : this.generateId();
    this._creationDate = Date.now();
    // this._expirationDate = this.getExpirationDate();
  }

  setAsProcessing() {
    //If a message is received by a consumer but NOT marked as processed within a configurable amount of time, the message then becomes available to any consumer requesting again.
    const { expirationTime } = config.queue.messages;

    console.log(this._id + " is set as processing");

    const message = queue._list[this._id];
    let i = queue._list.indexOf(message);
    if (i > -1) {
      queue._list.splice(i, 1);
      if (queue._list.hasOwnProperty(this._id)) {
        delete queue._list[this._id];
      }
    }

    queue._processing[this._id] = this;
    queue._processing.push(this);

    //in the time given by expirationTime in milliseconds this function will check if the message is stil pending
    setTimeout(() => {
      if (utils.findMessage(this._id, queue._processing)) {
        queueController.enqueue(this._data, this._id);

        console.log(
          this._id +
            " is in queue again, because has taken too much time to be processed."
        );
      }
    }, expirationTime);

    return true;
  }

  generateId() {
    return Math.random().toString(36).substring(config.idMessageSize);
  }
}
