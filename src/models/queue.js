import utils from "./../utils";

export default class Queue {
  //Creates a new queue instance and initializes the underlying data structure
  constructor() {
    this._list = [];
    this._processing = [];
    console.log("Queue instanced");
  }

  //Adds a new message at the end of the queue
  enqueue(message) {
    //redundancy data for avoid order n in future searchign
    let index = this._list.push(message);
    //console.log(index);
    this._list[message._id] = this._list[index];
    return message._id;
  }

  //this delete a messaje when is the processing list
  delete(id) {
    console.log(id);
    const message = this._processing[id];
    let i = this._processing.indexOf(message);
    console.log(i);
    if (i > -1) {
      this._processing.splice(i, 1);
      if (this._processing.hasOwnProperty(id)) {
        delete this._processing[id];
      }
      i = this._list.indexOf(message);
      if (i) {
        this._list.splice(i, 1);
      }
      if (this._list.hasOwnProperty(id)) {
        delete this._list[id];
      }

      console.log(id + " has been processed successfully");
      return true;
    } else {
      return false;
    }
  }

  // Removes the item from the front of the queue.
  dequeue() {
    if (this._list.length === 0) {
      return null;
    } else {
      const message = this._list[0];

      message.setAsProcessing();
      this._processing.push(message);
      console.log(message._id + " is being processed by consumer");
      console.log(
        (this._processing.length == 1 ? " message" : " messages") +
          " being processed"
      );

      this._list.shift();
      return message;
    }
  }

  clear() {
    this._list = [];
    this._processing = [];
    console.log("Queue is empty");
    return { message: "Queue is empty" };
  }
}
