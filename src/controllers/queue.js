import { config } from "../../config.js";
import { Message } from "./../models";

export default module.exports = {
  getState: () => {
    return {
      size: queue._list.length,
      nextMessaje: queue._list[0],
    };
  },

  enqueue: (data, id) => {
    let queueSize = queue._list.length;
    console.log(queue._list.length);
    //if queue is full
    if (queueSize >= config.queue.maxSize) {
      console.log("Queue is full " + queueSize);

      return { error: "Queue is full " + queueSize, code: "987" };
    } else {
      const message = new Message(data, id);
      const actionResponse = queue.enqueue(message);

      if (actionResponse) {
        queueSize++;
        console.log("Queue Size: " + queueSize);

        console.log(message["_id"] + " queued");

        return message["_id"];
      } else {
        console.log("Error: Mesage " + message["_id"] + "wasn't queued");

        return { code: "856", error: message["_id"] + "wasn't queued" };
      }
    }
  },

  dequeue: () => {
    const result = queue.dequeue();
    let queueSize = queue._list.length;
    console.log("Queue Size: " + queueSize);

    if (result) {
      return result;
    } else {
      return { error: "Queue is empty", code: "324" };
    }
  },

  delete: (id) => {
    queue._process
    if (queue.delete(id)) {
      return { message: id + " has been processed successfully" };
    } else {
     return { error: id + " is not set as in process", code: "293" }
        }
  },
  clear: () => {
    return queue.clear();
  },
};
