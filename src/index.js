import express from "express";
import bodyParser from "body-parser";

import routes from "./routes";
import { Queue } from "./models/";
import { config } from "./../config";
import log from "./../log";

const app = express();

// Middleware
app.use(bodyParser.json());

if (config.saveLog) {
  log();
}

//Instance a new Queue
global.queue = new Queue();

// * Routes * //
app.use("/messages", routes.messages);
app.use("/queue", routes.queue);

// * Start * //
app.listen(config.listenPort, () =>
  console.log(
    `WonderQ service is listening requests on port ${config.listenPort}`
  )
);
